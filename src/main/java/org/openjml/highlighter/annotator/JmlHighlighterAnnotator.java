package org.openjml.highlighter.annotator;

import com.intellij.lang.annotation.AnnotationHolder;
import com.intellij.lang.annotation.Annotator;
import com.intellij.lang.annotation.HighlightSeverity;
import com.intellij.openapi.editor.colors.TextAttributesKey;
import com.intellij.psi.PsiComment;
import com.intellij.psi.PsiElement;
import org.jetbrains.annotations.NotNull;

public class JmlHighlighterAnnotator implements Annotator {

    public static final TextAttributesKey JML_ANNOTATION = TextAttributesKey.createTextAttributesKey("JML_ANNOTATION");

    @Override
    public void annotate(@NotNull PsiElement element, @NotNull AnnotationHolder holder) {
        if (!(element instanceof PsiComment)) {
            return;
        }
        final String text = element.getText();
        if (text.startsWith("//@") || (text.startsWith("/*@") && text.endsWith("@*/"))) {
            holder.newAnnotation(HighlightSeverity.INFORMATION, element.getText())
                    .textAttributes(JML_ANNOTATION).create();
        }
    }

}
